/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <Add File Name> 
 * @brief <Add Brief Description Here >
 *
 * <Add Extended Description Here>
 *
 * @author <SImeon UWizeye>
 * @date <25 July 2020 >
 *
 */

#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

int main() /* The main entry point for your program */
{

  unsigned char test[SIZE] = {34, 201, 190, 154, 8, 194, 2, 6,
                              114, 88, 45, 76, 123, 87, 25, 23,
                              200, 122, 150, 90, 92, 87, 177, 244,
                              201, 6, 12, 60, 8, 2, 5, 67,
                              7, 87, 250, 230, 99, 3, 100, 90};
  unsigned char *sorted_array;

  print_statistics(test, SIZE);
  sort_array(test, SIZE);
  sorted_array = sort_array(test, SIZE);
  printf("\n the sorted array is:\n\n");
  print_array(sorted_array, SIZE);
}
void print_statistics(unsigned char array[], unsigned int size)
{
  unsigned int max_value = find_maximum(array, size);
  unsigned int min_value = find_minimum(array, size);
  unsigned int mean_value = find_mean(array, size);
  unsigned int median_value = find_median(array, size);
  printf("\nThe maximum of the array is: %u\n", max_value);
  printf("The minimum of the array is: %u\n", min_value);
  printf("The mean of the array is: %u\n", mean_value);
  printf("The median of the array is: %u\n", median_value);
}

void print_array(unsigned char array[], unsigned int size)
{
  for (int i = 0; i < size; i++)
  {
    printf("%u\n", array[i]);
  }
}
unsigned int find_median(unsigned char array[], unsigned int size)
{
  return floor(array[(size + 1) / 2]);
}
unsigned int find_mean(unsigned char array[], unsigned int size)
{
  int sum;
  for (int i = 0; i < size; i++)
  {
    sum += array[i];
  }
  return floor(sum / size);
}
unsigned int find_maximum(unsigned char array[], unsigned int size)
{
  int max = array[0];
  for (int i = 1; i < size; i++)
  {
    if (array[i] > max)
    {
      max = array[i];
    }
  }
  return floor(max);
}
unsigned int find_minimum(unsigned char array[], unsigned int size)
{
  int min = array[0];
  for (int i = 1; i < size; i++)
  {
    if (array[i] < min)
    {
      min = array[i];
    }
  }
  return floor(min);
}
unsigned char *sort_array(unsigned char array[], unsigned int size)
{
  int temp;
  for (int i = 1; i < size; ++i)
  {
    for (int j = i; j > 0; --j)
    {
      if (array[j] > array[j - 1])
      {
        temp = array[j];
        array[j] = array[j - 1];
        array[j - 1] = temp;
      }
    }
  }
  return array;
}
