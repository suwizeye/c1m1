# Week 1 Application Assignment

Thi is a simple application that performs statistical analytics on a dataset. 
We begin by setting up a version control repository on a local machine, develope and test the code.

Here is a list of the functions implemented in this application:

- **main()** - The main entry point for your program
- **print_statistics()** - A function that prints the statistics of an array including minimum, maximum, mean, and median.
- **print_array()** - Given an array of data and a length, prints the array to the screen
- **find_median()** - Given an array of data and a length, returns the median value
- **find_mean()** - Given an array of data and a length, returns the mean
- **find_maximum()** - Given an array of data and a length, returns the maximum
- **find_minimum()** - Given an array of data and a length, returns the minimum
- **sort_array()** - Given an array of data and a length, sorts the array from largest to smallest. (The zeroth Element should be the largest value, and the last element (n-1) should be the smallest value. )

# Author:

**Simeon Uwizeye**