/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <Add File Name> 
 * @brief Performing statistics on an array. >
 *
 *This application contains functions that can analyze an array of unsigned char data items and report analytics on the maximum, minimum, mean, and median of the data set.
 *It also contains a function to sort elements of an array from the largest to the smallest.
 *
 * @author <Simeon Uwizeye>
 * @date <26 July 2020>
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/**
 * @brief Print the statistics of an array including minimum, maximum, mean, and median.
 * 
 * This function takes as input an array of unsigned integers and and int of it's size.
 * It prints the statistics including minimum, maximum, mean and median of that array.
 * 
 * @param array array of unsigned integers 
 * @param size an integer which is the size of the array
 */
void print_statistics(unsigned char array[], unsigned int size);

/**
 * @brief Print the elements of an array.
 * 
 * This function takes as input an array of unsigned integers and and int of it's size.
 * It prints the elements of the array.
 * 
 * @param array array of unsigned integers 
 * @param size an integer which is the size of the array
 */
void print_array(unsigned char array[], unsigned int size);

/**
 * @brief a function that returns the median of the array.
 * 
 * This function takes as input an array of unsigned integers and and int of it's size.
 * This function returns the median whose position is got by halving the sum of the size of the array and one.
 * 
 * @param array array of unsigned integers 
 * @param size an integer which is the size of the array
 * 
 * @return The median of the array.
 */
unsigned int find_median(unsigned char array[], unsigned int size);

/**
 * @brief a function that returns the mean of the array.
 * 
 * This function takes as input an array of unsigned integers and and int of it's size.
 * This function returns the mean by halving the sum of the elements of the array.
 * 
 * @param array array of unsigned integers 
 * @param size an integer which is the size of the array
 * 
 * @return The mean of the array.
 */
unsigned int find_mean(unsigned char array[], unsigned int size);

/**
 * @brief a function that returns the mean of the array.
 * 
 * This function takes as input an array of unsigned integers and and int of it's size.
 * This function returns the maximum of the array. 
 * 
 * @param array array of unsigned integers 
 * @param size an integer which is the size of the array
 * 
 * @return The maximum of the array.
 */
unsigned int find_maximum(unsigned char array[], unsigned int size);

/**
 * @brief a function that returns the mean of the array.
 * 
 * This function takes as input an array of unsigned integers and and int of it's size.
 * This function returns the minimum of the array. 
 * 
 * @param array array of unsigned integers 
 * @param size an integer which is the size of the array
 * 
 * @return The minimum of the array.
 */
unsigned int find_minimum(unsigned char array[], unsigned int size);

/**
 * @brief a function that returns the mean of the array.
 * 
 * This function takes as input an array of unsigned integers and and int of it's size.
 * This function sorts the elements of the array using the insertion-sort algorithm from the highest to the kowest.
 * 
 * @param array array of unsigned integers 
 * @param size an integer which is the size of the array
 * 
 */
unsigned char *sort_array(unsigned char array[], unsigned int size);

#endif /* __STATS_H__ */
